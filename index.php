<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Cyber Sign</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/css/bootstrap.min.css"/>
    <link rel="stylesheet"
          href="./statics/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css"/>
    <!-- CSS -->
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Choose Type Sign</h3>
                </div>
                <div class="panel-body">
                    <form class="" method="POST" target="_blank" enctype="application/x-www-form-urlencoded"
                          action="init_payment.php">
                        <div class="col">
                            <div class="col-md-3">
                                <div class="form-group">
                                <a href="/SignPDF/sign.php" class="btn btn-primary">Sign PDF</a>
                                </div>
                            </div>
                            
                        </div>
                        <div class="col">
                            <div class="col-md-3">
                                <div class="form-group">
                                <a href="/" class="btn btn-primary">Sign XML</a>
                                </div>
                            </div>
                            
                        </div>
                        <div class="col">
                            <div class="col-md-3">
                                <div class="form-group">
                                <a href="/" class="btn btn-primary">Sign Office</a>
                                </div>
                            </div>
                            
                        </div>
                        <div class="col">
                            <div class="col-md-3">
                                <div class="form-group">
                                <a href="/" class="btn btn-primary">Sign JSOn</a>
                                </div>
                            </div>
                            
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<script type="text/javascript" src="./statics/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="./statics/moment/min/moment.min.js"></script>
<script type="text/javascript" src="./statics/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript"
        src="./statics/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

</body>
</html>