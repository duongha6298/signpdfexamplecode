<?php

class DataSign {
    public $base64pdf = "";
    public $hashalg ="";
    public $typesignature = "";
    public $signaturename ="";
    public $base64image ="";
    public $textout = "";
    public $pagesign = "";
    public $xpoint = "";
    public $ypoint = "";
    public $width ="";
    public $height = "";
}