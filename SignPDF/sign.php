<?php

header('Content-type: text/html; charset=utf-8');

include "DataSign.php";
include "HttpRequestMessage.php";

$secret = base64_decode('NDA2ZjNiOWUwYWI4NGYyODMxNGNlM2MwNWRhNGU3ZWZhYWNkODAzOWIwY2Q1ZmYxNGU4NGQyMzc4OTY2ZDlhMg==');
$api_id = "b528cb04b9094a6fa8cb04b9093a6f66";
$path = "http://192.168.100.214:8088/api/pdf/sign/originaldata";

$nonce = bin2hex(random_bytes(16));
$date = date(\DateTime::RFC2822);
$date = str_replace('+0000', 'GMT', $date);

if (isset($_POST['Upload'])) {
    if (isset($_FILES['pdfFile'])) {
        if ($_FILES['pdfFile']['type'] == "application/pdf") {
            $source_file = $_FILES['pdfFile']['tmp_name'];
            $dest_file = "FileUpload/" . $_FILES['pdfFile']['name'];

            if (file_exists($dest_file)) {
                echo "<script>alert('The file name already exists!!!');</script>";
            } else {
                move_uploaded_file($source_file, $dest_file)
                or die("Error!!");
                if ($_FILES['pdfFile']['error'] == 0) {

                    $filePath = "FileUpload/" . $_FILES['pdfFile']['name'];

                    $hnd = fopen($filePath, 'rb');
                    $data = fread($hnd, filesize($filePath));
                    $base64pdf = base64_encode($data);

                    echo $base64pdf;

                    fclose($hnd);
                }
            }
        } else {
            if ($_FILES['pdfFile']['type'] != "application/pdf") {
                echo "<script>alert('Invalid  file extension, should be pdf !!!');</script>";
            }
        }
    }
}

if (!empty($_POST['Sign'])) {
    $base64pdf = $_POST["base64pdf"];
    $hashalg = $_POST["hashalg"];
    $typesignature = $_POST["typesignature"];
    $signaturename = $_POST["signaturename"];
    $base64image = $_POST["base64image"];
    $textout = $_POST["textout"];
    $pagesign = $_POST["pagesign"];
    $xpoint = $_POST["xpoint"];
    $ypoint = $_POST["ypoint"];
    $width = $_POST["width"];
    $height = $_POST["height"];

    if (empty($base64pdf)) {
        echo "<script>alert('Base64 Pdf dose not exists!!!');</script>";
    } else {
        $httpRequest = new HttpRequestMessage($base64pdf);

        $data = new DataSign();
        $data->base64pdf = $httpRequest->ContentPdf;
        $data->hashalg = $hashalg;
        $data->typesignature = $typesignature;
        $data->signaturename = $signaturename;
        $data->base64image = $base64image;
        $data->textout = $textout;
        $data->pagesign = $pagesign;
        $data->xpoint = $xpoint;
        $data->ypoint = $ypoint;
        $data->width = $width;
        $data->height = $height;

        $data_json = json_encode($data);

        $signatureRawData = $httpRequest->Method
        . "\n" .
        $httpRequest->RequestUri_Scheme
        . "\n" .
        $httpRequest->RequestUri_Host . ":" . $httpRequest->RequestUri_Port
        . "\n" .
        $httpRequest->RequestUri_LocalPath
        . "\n" .
        $httpRequest->Content_Headers_ContentType
            . "\n" .
            $api_id
            . "\n" .
            $nonce
            . "\n" .
            $date
            . "\n" .
            $data_json
            . "\n"
        ;

        $signature = hash_hmac('sha256', $signatureRawData, $secret, true);

        $sign_base64 = base64_encode($signature);

        $timeStamp = strtotime($date);

        $headers = array(
            'Content-Type: application/json',
            'Authorization: HmacSHA256 ' . $api_id . ":" . $nonce . ":" . $sign_base64 . ":" . $timeStamp,
            'Date: ' . $date,
        );

        $ch = curl_init($path);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
        $res = curl_exec($ch);
        curl_close($ch);

        $jsonResult = json_decode($res, true); // decode json

        // echo "</br>Textout: ".$textout."</br>";
        // echo "</br>Datajson:" .var_dump($data)."</br>";
        // echo "</br>signatureRawData: " . $signatureRawData . "</br>";
        // echo "</br>sign_base64: " . $sign_base64 . "</br>";

        // echo "</br>headers: " . implode(" ", $headers) . "</br>";
        // echo "</br>data_js: " . serialize($data_json) . "</br>";
        // echo "</br>path: " . $path . "</br>";

        // echo "</br>res: " . $res . "</br>";

        // echo "</br> result: " . implode("", $jsonResult) . "</br>";

        // echo "</br> nonce: " . $nonce . "</br>";
        // echo "</br> secret: " . $secret . "</br>";

        // echo "</br>jsonResultBase64: " . $jsonResult['base64pdfSigned'] . "</br>";
        // echo "</br> Status:" . $jsonResult['status'] . "</br>";
        echo "<script>alert('Successfully!!!');</script>";
    }

}
?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Cyber Sign</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/css/bootstrap.min.css"/>
    <link rel="stylesheet"
          href="./statics/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css"/>
    <!-- CSS -->
</style>
</head>
<body>
<div class="container">
<div class="row">
        <div class="col-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Data Signs</h3>
                    <form enctype="multipart/form-data" action="sign.php" method="POST" enctype='multipart/form-data'>

                            <input type="file" name="pdfFile" value="" style="float: right; margin-top: -1em"/>
                            <br/>
                            <br/>
                            <input type="submit" value="Upload" name="Upload" style="float: right; margin-top: -1.5em; margin-right: 14.6em" />

	                </form>
                </div>
                <div class="panel-body">
                          <form target="_blank" enctype="application/x-www-form-urlencoded" action="sign.php" method="POST" >
                        <div class="row">

                        <div class="col-md-4">
                                <div class="form-group">
                                    <label for="fxRate" class="col-form-label">Base64 Pdf</label>
                                    <div class='input-group date' id='fxRate'>
                                    <input type="text" name="base64pdf" value=""
                                               class="form-control"/>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="fxRate" class="col-form-label">Hashalg</label>
                                    <div class='input-group date' id='fxRate'>
                                        <input type='text' name="hashalg" value=""
                                               class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="fxRate" class="col-form-label">Type Signature</label>
                                    <div class='input-group date' id='fxRate'>
                                        <input type='text' name="typesignature" value="3"
                                               class="form-control"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="fxRate" class="col-form-label">Signature Name</label>
                                    <div class='input-group date' id='fxRate'>
                                        <input type='text' name="signaturename" value="CyberLotus"
                                               class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="fxRate" class="col-form-label">Base64 Image</label>
                                    <div class='input-group date' id='fxRate'>
                                        <input type='text' name="base64image"
                                               class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="fxRate" class="col-form-label">Textout</label>
                                    <div class='input-group date' id='fxRate'>
                                        <input type='text' name="textout" value="CyberSign"
                                               class="form-control"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="fxRate" class="col-form-label">Page Sign </label>
                                    <div class='input-group date' id='fxRate'>
                                        <input type='text' name="pagesign" value="1"  class="form-control"/>

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="fxRate" class="col-form-label">xPoint</label>
                                    <div class='input-group date' id='fxRate'>
                                        <input type='text' name="xpoint" value="100"
                                               class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="fxRate" class="col-form-label">yPoint</label>
                                    <div class='input-group date' id='fxRate'>
                                        <input type='text' name="ypoint" value="100"
                                               class="form-control"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="fxRate" class="col-form-label">Width </label>
                                    <div class='input-group date' id='fxRate'>
                                        <input type='text' name="width" value="200" class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="fxRate" class="col-form-label">Height</label>
                                    <div class='input-group date' id='fxRate'>
                                        <input type='text' name="height" value="100"
                                               class="form-control"/>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <p>
                        <div style="margin-top: 1em;">
                            <input type="submit" value="Sign" name="Sign"  class="btn btn-primary btn-block"/>
                        </div>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
    </div>
<script type="text/javascript" src="./statics/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="./statics/moment/min/moment.min.js"></script>
<script type="text/javascript" src="./statics/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript"
        src="./statics/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>



</body>
</html>
