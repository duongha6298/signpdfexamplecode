<?php

class HttpRequestMessage
{
    public $RequestUri = "http://192.168.100.214:8088/api/pdf/sign/originaldata";
    public $RequestUri_Scheme = "http";
    public $RequestUri_Host = "192.168.100.214";
    public $RequestUri_Port = "8088";
    public $RequestUri_LocalPath = "/api/pdf/sign/originaldata";
    public $Method = "POST"; // GET or POST
    public $ContentPdf = "";

    public $Content_Headers_ContentType = "application/json";

    public function __construct($fileContent)
    {
        $this->ContentPdf = $fileContent;
    }

}
